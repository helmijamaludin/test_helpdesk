<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Helpdesk</title>
    <link rel="icon" sizes="128x128" href="master/img/logo1.png">
    <link href="master/css/bootstrap.min.css" rel="stylesheet">
    <link href="master/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="master/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <!-- Gritter -->
    <link href="master/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    <link href="master/css/animate.css" rel="stylesheet">
    <link href="master/css/style.css" rel="stylesheet">
</head>

<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <h1><strong>Helpdesk</strong>
                            <img alt="image" class="rounded-circle" src="master/img/logo1.png"width="15%"></h1>
                        </div>
                    </li>
                    <li class="">
                        <a href="welcome.php"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                    <li class="">
                        <a href="create_ticket.php"><i class="fa fa-ticket"></i> <span class="nav-label">New Ticket</span></a>
                    </li>
                    <li class="">
                        <a href="view.php"><i class="fa fa-eye"></i> <span class="nav-label">View Ticket</span></a>
                    </li>
                  
                </ul>
            </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <div style="text-align: center;"><?php echo " " . date("d/m/Y") . "<br>";?></div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <!--<form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>-->
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li style="padding: 20px">
                    <span class="m-r-sm text-muted welcome-message">Welcome <?php echo $_SESSION["username"]; ?></span>
                </li>
               
                <li>
                    <a href="logout.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
        <div class="#viewticket">
            <h1>View Ticket</h1>
        </div>

       
    <div class="row justify-content-center">
        <table class="table" style="text-align: center;">
        <tr>
            <th >Title</th>
            <th>Subject</th>
            <th>Description</th>
            <th>Category</th>
            
        </tr>
        <?php
        $conn = mysqli_connect("localhost", "root","", "login_system");
        $sql = "SELECT * FROM newticket ORDER BY id DESC;";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()){ 
                echo "<tr><td>". $row["title"] . "</td>
                          <td>". $row["subject"] . "</td>
                          <td>". $row["description"] . "</td>
                          <td>". $row["category"] . "</td>
                      </tr>";
            }
        }else{
            echo "No result";
        }
        $conn->close();

        ?>
        </table>
    </div>
       
    
       
            <div class="footer">
                <!--<div class="float-right">
                    10GB of <strong>250GB</strong> Free.
                </div>-->
                <div>
                    <center>Helpdesk &copy; 2020</center>


                </div>


            </div>
        </div>
        <div class="small-chat-box fadeInRight animated">

            
        </div>
        <div id="small-chat">

            <span class="badge badge-warning float-right">1</span>
            <a class="open-small-chat" href="">
                <i class="fa fa-comments"></i>

            </a>
        </div>
       
    </div>

    <!-- Mainly scripts -->
    <script src="master/js/jquery-3.1.1.min.js"></script>
    <script src="master/js/popper.min.js"></script>
    <script src="master/js/bootstrap.js"></script>
    <script src="master/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="master/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="master/js/plugins/flot/jquery.flot.js"></script>
    <script src="master/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="master/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="master/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="master/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="master/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="master/js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="master/js/inspinia.js"></script>
    <script src="master/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="master/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="master/js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="master/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="master/js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="master/js/plugins/chartJs/Chart.min.js"></script>

    <!-- Toastr -->
    <script src="master/js/plugins/toastr/toastr.min.js"></script>


    <script>
        /*$(document).ready(function() {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('Responsive Admin', 'Welcome to Dashboard');

            }, 1300);


            var data1 = [
                [0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,30],[11,10],[12,13],[13,4],[14,3],[15,3],[16,6]
            ];
            var data2 = [
                [0,1],[1,0],[2,2],[3,0],[4,1],[5,3],[6,1],[7,5],[8,2],[9,3],[10,2],[11,1],[12,0],[13,2],[14,8],[15,0],[16,0]
            ];
            $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [
                data1, data2
            ],
                    {
                        series: {
                            lines: {
                                show: false,
                                fill: true
                            },
                            splines: {
                                show: true,
                                tension: 0.4,
                                lineWidth: 1,
                                fill: 0.4
                            },
                            points: {
                                radius: 0,
                                show: true
                            },
                            shadowSize: 2
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#d5d5d5",
                            borderWidth: 1,
                            color: '#d5d5d5'
                        },
                        colors: ["#1ab394", "#1C84C6"],
                        xaxis:{
                        },
                        yaxis: {
                            ticks: 4
                        },
                        tooltip: false
                    }
            );

            var doughnutData = {
                labels: ["App","Software","Laptop" ],
                datasets: [{
                    data: [300,50,100],
                    backgroundColor: ["#a3e1d4","#dedede","#9CC3DA"]
                }]
            } ;


            var doughnutOptions = {
                responsive: false,
                legend: {
                    display: false
                }
            };


            var ctx4 = document.getElementById("doughnutChart").getContext("2d");
            new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});

            var doughnutData = {
                labels: ["App","Software","Laptop" ],
                datasets: [{
                    data: [70,27,85],
                    backgroundColor: ["#a3e1d4","#dedede","#9CC3DA"]
                }]
            } ;


            var doughnutOptions = {
                responsive: false,
                legend: {
                    display: false
                }
            };


            var ctx4 = document.getElementById("doughnutChart2").getContext("2d");
            new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});

        });*/
    </script>
</body>
</html>
