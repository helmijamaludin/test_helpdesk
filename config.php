<?php
/* Database credentials. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'login_system');

$db = new PDO('mysql:host=localhost;dbname=' . DB_NAME . ';charset=utf8', DB_USERNAME, DB_PASSWORD);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
/* Attempt to connect to MySQL database */
$connection_db = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
 
 //Check connection
if(!$connection_db){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

/*$server = "localhost";
$user = "root";
$pass = "";
$database = "login_system";

$conn = mysqli_connect($server, $user, $pass, $database);

if (!$conn) {
    die("<script>alert('Connection Failed.')</script>");
}*/
?>